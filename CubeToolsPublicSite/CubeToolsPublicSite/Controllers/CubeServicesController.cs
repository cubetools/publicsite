﻿using _4Log.Log;
using log4net;
using System;
using System.Web.Mvc;

namespace CubeToolsPublicSite.Controllers
{
	public class CubeServicesController : BaseController
	{
		// GET: CubeServices
		public ActionResult Index()
		{
			try
			{
				ViewBag.Type = "CubeServices";
				return View();
			}

			catch (Exception ex)
			{
				ModelState.AddModelError("error", ex.Message);
				LogObject.WriteErrorLogWithMessage("Error in CubeServices Index", ex);
				return View();
			}

		}
		public ActionResult Overview()
		{
			try
			{
				return View();
			}

			catch (Exception ex)
			{
				ModelState.AddModelError("error", ex.Message);
				LogObject.WriteErrorLogWithMessage("Error in CubeServices Overview", ex);
				return View();
			}
		}

		public ActionResult CubeVolatilityOverview()
		{
			try
			{
				return View();
			}

			catch (Exception ex)
			{
				ModelState.AddModelError("error", ex.Message);
				LogObject.WriteErrorLogWithMessage("Error in CubeServices CubeVolatilityOverview", ex);
				return View();
			}
		}
		public ActionResult CubeVolatilityFeatures()
		{
			try
			{
				return View();
			}

			catch (Exception ex)
			{
				ModelState.AddModelError("error", ex.Message);
				LogObject.WriteErrorLogWithMessage("Error in CubeServices CubeVolatilityFeatures", ex);
				return View();
			}
		}

		public ActionResult CubeMarginsOverview()
		{
			try
			{
				return View();
			}

			catch (Exception ex)
			{
				ModelState.AddModelError("error", ex.Message);
				LogObject.WriteErrorLogWithMessage("Error in CubeServices CubeMarginsOverview", ex);
				return View();
			}
		}

		public ActionResult CubeMarginsFeatures()
		{
			try
			{
				return View();
			}

			catch (Exception ex)
			{
				ModelState.AddModelError("error", ex.Message);
				LogObject.WriteErrorLogWithMessage("Error in CubeServices CubeMarginsFeatures", ex);
				return View();
			}
		}

		public ActionResult VaRAnalysisOverview()
		{
			try
			{
				return View();
			}

			catch (Exception ex)
			{
				ModelState.AddModelError("error", ex.Message);
				LogObject.WriteErrorLogWithMessage("Error in CubeServices VaRAnalysisOverview", ex);
				return View();
			}
		}
		public ActionResult VaRAnalysisFeatures()
		{
			try
			{
				return View();
			}

			catch (Exception ex)
			{
				ModelState.AddModelError("error", ex.Message);
				LogObject.WriteErrorLogWithMessage("Error in CubeServices VaRAnalysisFeatures", ex);
				return View();
			}
		}

		public ActionResult SensitivityAnalysisOverview()
		{
			try
			{
				return View();
			}

			catch (Exception ex)
			{
				ModelState.AddModelError("error", ex.Message);
				LogObject.WriteErrorLogWithMessage("Error in CubeServices SensitivityAnalysisOverview", ex);
				return View();
			}

		}
		public ActionResult SensitivityAnalysisFeatures()
		{
			try
			{
				return View();
			}

			catch (Exception ex)
			{
				ModelState.AddModelError("error", ex.Message);
				LogObject.WriteErrorLogWithMessage("Error in CubeServices SensitivityAnalysisFeatures", ex);
				return View();
			}

		}

		public ActionResult CubeFinanceTools()
		{
			try
			{
				return View();
			}

			catch (Exception ex)
			{
				ModelState.AddModelError("error", ex.Message);
				LogObject.WriteErrorLogWithMessage("Error in CubeServices CubeFinanceTools", ex);
				return View();
			}

		}

		public ActionResult CubeGreeksOverview()
		{
			try
			{
				return View();
			}

			catch (Exception ex)
			{
				ModelState.AddModelError("error", ex.Message);
				LogObject.WriteErrorLogWithMessage("Error in CubeServices CubeGreeksOverview", ex);
				return View();
			}
		}
		public ActionResult CubeGreeksFeatures()
		{
			try
			{
				return View();
			}

			catch (Exception ex)
			{
				ModelState.AddModelError("error", ex.Message);
				LogObject.WriteErrorLogWithMessage("Error in CubeServices CubeGreeksFeatures", ex);
				return View();
			}
		}
		public ActionResult CubeAPIMarginsOverview()
		{
			try
			{
				return View();
			}

			catch (Exception ex)
			{
				ModelState.AddModelError("error", ex.Message);
				LogObject.WriteErrorLogWithMessage("Error in CubeServices CubeAPIMarginsOverview", ex);
				return View();
			}
		}
		public ActionResult CubeAPIMarginsFeatures()
		{
			try
			{
				return View();
			}

			catch (Exception ex)
			{
				ModelState.AddModelError("error", ex.Message);
				LogObject.WriteErrorLogWithMessage("Error in CubeServices CubeAPIMarginsFeatures", ex);
				return View();
			}
		}
	}
}