﻿using System;
using System.Web;
using System.Web.Mvc;
using CubeToolsPublicSite.Helpers;
using System.Threading;
using log4net;
using System.Net;
using System.Linq;
using _4Log.Log;

namespace CubeToolsPublicSite.Controllers
{
    public class BaseController : Controller
    {
        protected override IAsyncResult BeginExecuteCore(AsyncCallback callback, object state)
        {
            string cultureName = null;

            // Attempt to read the culture cookie from Request
            HttpCookie cultureCookie = Request.Cookies["_culture"];
            if (cultureCookie != null)
                cultureName = cultureCookie.Value;
            else
                cultureName = Request.UserLanguages != null && Request.UserLanguages.Length > 0 ? Request.UserLanguages[0] : null; // obtain it from HTTP header AcceptLanguages

            // Validate culture name
            cultureName = CultureHelper.GetImplementedCulture(cultureName); // This is safe


            // Modify current thread's cultures            
            Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(cultureName);
            Thread.CurrentThread.CurrentUICulture = Thread.CurrentThread.CurrentCulture;


            return base.BeginExecuteCore(callback, state);
        }

		protected override void OnException(ExceptionContext filterContext)
		{
			//Log error
			LogObject.WriteErrorLogWithMessage(filterContext.Exception.Message, filterContext.Exception);

			var controllerName = (string)filterContext.RouteData.Values["controller"];
			var actionName = (string)filterContext.RouteData.Values["action"];
			if (filterContext.ExceptionHandled || !filterContext.HttpContext.IsCustomErrorEnabled)
			{

				filterContext.Result = new ViewResult
				{
					ViewName = actionName,
					MasterName = controllerName,
					// ViewData = new ViewDataDictionary<HandleErrorInfo>(model),
					TempData = filterContext.Controller.TempData
				};
				return;
			}

			if (new HttpException(null, filterContext.Exception).GetHttpCode() != 500)
			{
				return;
			}

			//if (!ExceptionType.IsInstanceOfType(filterContext.Exception))
			//{
			//    return;
			//}

			// if the request is AJAX return JSON else view.
			if (filterContext.HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest")
			{
				filterContext.Result = new JsonResult
				{
					JsonRequestBehavior = JsonRequestBehavior.AllowGet,
					Data = new
					{
						error = true,
						result = "ERROR",
						message = filterContext.Exception.Message
					}
				};
			}
			else
			{
				if (filterContext.Exception is HttpException)
				{
					var errorCode = ((HttpException)filterContext.Exception).ErrorCode;

					if (errorCode == -2147467259)
					{
						controllerName = (string)filterContext.RouteData.Values["controller"];
						actionName = (string)filterContext.RouteData.Values["action"];
						var model = new HandleErrorInfo(((HttpException)filterContext.Exception), controllerName, actionName);

						filterContext.Result = new ViewResult
						{
							ViewName = "~/Views/Shared/Error.cshtml",
							// MasterName = Master,
							ViewData = new ViewDataDictionary<HandleErrorInfo>(model),
							TempData = filterContext.Controller.TempData
						};
					}
				}
			}
			filterContext.ExceptionHandled = true;
			filterContext.HttpContext.Response.Clear();
			filterContext.HttpContext.Response.StatusCode = 500;

			filterContext.HttpContext.Response.TrySkipIisCustomErrors = true;
			base.OnException(filterContext);

		}
	}
}