﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CubeToolsPublicSite.Helpers;
using System.Globalization;
using SharedResources;
using CubeToolsPublicSite.Models;
using log4net;
using System.Text;
using System.Text.RegularExpressions;
using _4Log.Log;

namespace CubeToolsPublicSite.Controllers
{
	public class HomeController : BaseController
	{
		readonly CubeFinanceToolsEntities cubeFinanceToolsEntities = new CubeFinanceToolsEntities();
		//readonly ILog logger = LogManager.GetLogger(typeof(HomeController));
		[HttpGet]
		public ActionResult Index()
		{
			try
			{
				ViewBag.Type = "Home";
				if (TempData["SetCultureException"] != null)
				{
					ModelState.AddModelError("error", TempData["SetCultureException"].ToString());
					return View();
				}
				else
				{
					
					LogObject.WriteInfoLog("Get Home Page");
				}
				
				return View();
			}
			catch (Exception ex)
			{
				ModelState.AddModelError("error", ex.Message);
				LogObject.WriteErrorLogWithMessage("Error in HomeController Index", ex);
				return View();
			}

		}
		public ActionResult SetCulture(string culture)
		{
			try
			{
				
				// Validate input
				var cultureValue = CultureHelper.GetImplementedCulture(culture);
				// Save culture in a cookie
				HttpCookie cookie = Request.Cookies["_culture"];
				if (cookie != null)
					cookie.Value = cultureValue;   // update cookie value
				else
				{
					cookie = new HttpCookie("_culture");
					cookie.Value = cultureValue;
					cookie.Expires = DateTime.Now.AddHours(24);
				}
				Response.Cookies.Add(cookie);
				return Redirect(Request.UrlReferrer.ToString());
			}
			catch (Exception ex)
			{
				LogObject.WriteErrorLogWithMessage("error in finding the culture nature", ex);
				TempData["SetCultureException"] = ex.Message;
				return RedirectToAction("Index");
				
			}
		}
		[HttpGet]
		public ActionResult ContactUs()
		{
			try
			{
				HttpCookie cookie = Request.Cookies["_culture"];
				CultureInfo myCultureInfo = new CultureInfo(cookie.Value);
				var issuerList = new List<SelectListItem>();
				var companyList = new List<SelectListItem>();
				var issuerTypeList = Resources.ResourceManager.GetString("IssuerTypeList", myCultureInfo).Split(',').ToList();
				var companyTypeList = Resources.ResourceManager.GetString("CompanyTypeList", myCultureInfo).Split(',').ToList();

				for (int i = 0; i < issuerTypeList.Count; i++)
				{
					issuerList.Add(new SelectListItem { Text = issuerTypeList[i], Value = issuerTypeList[i] });
				}
				for (int i = 0; i < companyTypeList.Count; i++)
				{
					companyList.Add(new SelectListItem { Text = companyTypeList[i], Value = companyTypeList[i] });
				}
				ContactusModel objContactusModel = new ContactusModel();
				objContactusModel.SelectMessage = Resources.ResourceManager.GetString("SelectMessage", myCultureInfo);
				objContactusModel.CompanyType = Resources.ResourceManager.GetString("CompanyType", myCultureInfo);
				objContactusModel.IssuerType = Resources.ResourceManager.GetString("IssuerType", myCultureInfo);
				objContactusModel.IssuerTypeList = issuerList;
				objContactusModel.CompanyTypeList = companyList;

				return View(objContactusModel);
			}
			catch (Exception ex)
			{
				ModelState.AddModelError("error", ex.Message);
				LogObject.WriteErrorLogWithMessage("error in sending the query ", ex);
				return View();
			}
		}
		[HttpPost]
		[ValidateInput(true)]
		public ActionResult ContactUs(ContactusModel objContactusModel)
		{

			string mailStatus = string.Empty;

			HttpCookie cookie = Request.Cookies["_culture"];
			CultureInfo myCultureInfo = new CultureInfo(cookie.Value);
			var issuerList = new List<SelectListItem>();
			var companyList = new List<SelectListItem>();
			var issuerTypeList = Resources.ResourceManager.GetString("IssuerTypeList", myCultureInfo).Split(',').ToList();
			var companyTypeList = Resources.ResourceManager.GetString("CompanyTypeList", myCultureInfo).Split(',').ToList();

			for (int i = 0; i < issuerTypeList.Count; i++)
			{
				issuerList.Add(new SelectListItem { Text = issuerTypeList[i], Value = issuerTypeList[i] });
			}
			for (int i = 0; i < companyTypeList.Count; i++)
			{
				companyList.Add(new SelectListItem { Text = companyTypeList[i], Value = companyTypeList[i] });
			}

			objContactusModel.SelectMessage = Resources.ResourceManager.GetString("SelectMessage", myCultureInfo);
			objContactusModel.CompanyType = Resources.ResourceManager.GetString("CompanyType", myCultureInfo);
			objContactusModel.IssuerType = Resources.ResourceManager.GetString("IssuerType", myCultureInfo);
			objContactusModel.IssuerTypeList = issuerList;
			objContactusModel.CompanyTypeList = companyList;


			try
			{
				if (ModelState.IsValid)
				{
					string ContactEmailTo = System.Configuration.ConfigurationManager.AppSettings["ContactEmailto"].ToString();
					List<String> customerTo = new List<String>();
					List<String> userTo = new List<String>();
					ContactU contactusModel = new ContactU();
					contactusModel.Name = objContactusModel.Name;
					contactusModel.Surname = objContactusModel.Surname;
					contactusModel.CompanyName = objContactusModel.CompanyName;
					contactusModel.IssuerType = objContactusModel.IssuerType;
					contactusModel.Email = objContactusModel.Email;
					contactusModel.CompanyType = objContactusModel.CompanyType;
					contactusModel.Message = objContactusModel.Message;
					contactusModel.MobileNumber = objContactusModel.Mobile;
					contactusModel.CreateDate = DateTime.UtcNow;


					/// for customer support
					//StringBuilder builder = new StringBuilder();
					customerTo.Add(ContactEmailTo);
					string supportMailBody = Utility.GetHtml(ControllerContext, "GetContactusHtml", contactusModel);

					string supportMailSubject = Utility.GetHtmlTitle(supportMailBody);
					mailStatus = SendEmail.SendEmailNew(customerTo, new List<string>(), new List<string>(), new List<string>(), new List<string>(), new List<string>(), null, null, supportMailSubject, supportMailBody, new List<string>());

					/// for user
					var templateTypeId = cubeFinanceToolsEntities.TemplateTypes.Where(t => t.TemplateTypeDesc == "Contacts Us").FirstOrDefault();
					var templateBody = cubeFinanceToolsEntities.ContactFormTemplates.Where(t => t.TemplateType.Contains(templateTypeId.TemplateTypeId.ToString())).FirstOrDefault();


					userTo.Add(objContactusModel.Email);

					string userMailbody = Utility.GetHtml(ControllerContext, "GetCustomerHtml", contactusModel);

					string userMailTemplatebody = templateBody.TemplateBody.Replace("#UserName#", CultureInfo.InvariantCulture.TextInfo.ToTitleCase(contactusModel.Name));

					userMailTemplatebody = userMailbody.Replace("#", userMailTemplatebody);

					string userMailSubject = Utility.GetHtmlTitle(userMailTemplatebody);
					mailStatus = SendEmail.SendEmailNew(userTo, new List<string>(), new List<string>(), new List<string>(), new List<string>(), new List<string>(), null, null, userMailSubject, userMailTemplatebody, new List<string>());

					if (mailStatus == "true")
					{
						//Store information in table.
						cubeFinanceToolsEntities.ContactUS.Add(contactusModel);
						cubeFinanceToolsEntities.SaveChanges();
						//
						ViewBag.Status = mailStatus;
					}

				}

				return View("ContactUs", objContactusModel);
			}
			catch (Exception ex)
			{
				ModelState.AddModelError("error", ex.Message);
				mailStatus = "error";
				LogObject.WriteErrorLogWithMessage("error in sending the mail ", ex);
				ViewBag.Status = mailStatus;
				return View("ContactUs", objContactusModel);

			}

		}
		//public JsonResult ContactUsEmail(string name, string surname, string companyName, string issuerType, string email, string companyType, string message)
		//{
		//	string mailStatus = string.Empty;
		//	try
		//	{
		//		string EmailTo = System.Configuration.ConfigurationManager.AppSettings["EmailTo"].ToString();
		//		List<String> customerTo = new List<String>();
		//		List<String> userTo = new List<String>();
		//		ContactU contactusModel = new ContactU();
		//		contactusModel.Name = name;
		//		contactusModel.Surname = surname;
		//		contactusModel.CompanyName = companyName;
		//		contactusModel.IssuerType = issuerType;
		//		contactusModel.Email = email;
		//		contactusModel.CompanyType = companyType;
		//		contactusModel.Message = message;
		//		contactusModel.CreateDate = DateTime.UtcNow;

		//		/// for customer support
		//		customerTo.Add(EmailTo);
		//		string supportMailBody = Utility.GetHtml(ControllerContext, "GetContactusHtml", contactusModel);
		//		string supportMailSubject = Utility.GetHtmlTitle(supportMailBody);
		//		mailStatus = SendEmail.SendEmailNew(customerTo, new List<string>(), new List<string>(), new List<string>(), new List<string>(), new List<string>(), null, null, supportMailSubject, supportMailBody, new List<string>());

		//		/// for user
		//		userTo.Add(email);
		//		string userMailbody = Utility.GetHtml(ControllerContext, "GetCustomerHtml", contactusModel);
		//		string userMailSubject = Utility.GetHtmlTitle(userMailbody);
		//		mailStatus = SendEmail.SendEmailNew(userTo, new List<string>(), new List<string>(), new List<string>(), new List<string>(), new List<string>(), null, null, userMailSubject, userMailbody, new List<string>());

		//		if (mailStatus == "true")
		//		{
		//			//Store information in table.
		//			cubeFinanceToolsEntities.ContactUS.Add(contactusModel);
		//			cubeFinanceToolsEntities.SaveChanges();
		//			//
		//		}

		//	}
		//	catch (Exception ex)
		//	{
		//		mailStatus = "error";
		//		LogObject.WriteErrorLogWithMessage("error in sending the mail ", ex);
		//		return Json(new { mailStatusValue = mailStatus, titleValue = Resources.ErrorTitle, textValue = Resources.ErrorText }, JsonRequestBehavior.AllowGet);
		//	}
		//	return Json(new { mailStatusValue = mailStatus, titleValue = Resources.ErrorTitle, textValue = Resources.ErrorText }, JsonRequestBehavior.AllowGet);
		//}

		public JsonResult AlertConfiguration(string status)
		{
			string title = string.Empty;
			string text = string.Empty;
			string statusType = string.Empty;
			try
			{
				if (!string.IsNullOrEmpty(status))
				{
					if (status == "true")
					{
						statusType = "true";
						title = Resources.SuccessTitle;
						text = Resources.SuccessText;
					}
					else if (status == "false")
					{
						statusType = "false";
						title = Resources.ErrorTitle;
						text = Resources.ErrorText;
					}
					else if (status == "error")
					{
						statusType = "error";
						title = Resources.ErrorTitle;
						text = Resources.ErrorText;
					}
				}
				return Json(new { statusTypeValue = statusType, titleValue = title, textValue = text }, JsonRequestBehavior.AllowGet);
			}
			catch (Exception ex)
			{
				LogObject.WriteErrorLogWithMessage("Error in HomeController AlertConfiguration", ex);
				return Json("", JsonRequestBehavior.AllowGet);
			}
		}

		public ActionResult TopMenus()
		{
			Menus menus = new Menus();
			try
			{
				return PartialView("_TopNavbar", menus);
			}
			catch (Exception ex)
			{
				LogObject.WriteErrorLogWithMessage("Error in HomeController TopMenus", ex);
				return null;
			}
		}

		public ActionResult InnerTopMenus()
		{
			Menus menus = new Menus();
			try
			{
				return PartialView("_TopNavbar_1", menus);
			}
			catch (Exception ex)
			{
				LogObject.WriteErrorLogWithMessage("Error in HomeController InnerTopMenus", ex);
				return null;
			}
		}

		public ActionResult PrivacyPolicy()
		{
			ViewBag.LagalPrivacy = true;
			return View();
		}
		public ActionResult LegalNotice()
		{
			ViewBag.LagalPrivacy = true;
			return View();
		}
   }
}