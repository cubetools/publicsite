﻿using _4Log.Log;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CubeToolsPublicSite.Controllers
{

	public class ConsultingController : BaseController
	{
		// GET: Consulting
		public ActionResult Index()
		{
			try
			{
				ViewBag.Type = "Consulting";
				return View();
			}

			catch (Exception ex)
			{
				ModelState.AddModelError("error", ex.Message);
				LogObject.WriteErrorLogWithMessage("Consulting Index", ex);
				return View();
			}
		}
		public ActionResult Overview()
		{
			try
			{
				return View();
			}

			catch (Exception ex)
			{
				ModelState.AddModelError("error", ex.Message);
				LogObject.WriteErrorLogWithMessage("Consulting Overview", ex);
				return View();
			}
		}
		public ActionResult RiskManagementOutsourcing()
		{
			try
			{
			
				return View();
			}

			catch (Exception ex)
			{
				ModelState.AddModelError("error", ex.Message);
				LogObject.WriteErrorLogWithMessage("Consulting RiskManagementOutsourcing", ex);
				return View();
			}
		}
		public ActionResult RiskManagementConsulting()
		{
			try
			{
				return View();
			}

			catch (Exception ex)
			{
				ModelState.AddModelError("error", ex.Message);
				LogObject.WriteErrorLogWithMessage("Consulting RiskManagementConsulting", ex);
				return View();
			}
		}

		public ActionResult InvestmentAdvisory()
		{
			try
			{
				return View();
			}

			catch (Exception ex)
			{
				ModelState.AddModelError("error", ex.Message);
				LogObject.WriteErrorLogWithMessage("Consulting InvestmentAdvisory", ex);
				return View();
			}
		}

	}
}