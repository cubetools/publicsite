﻿using _4Log.Log;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using log4net;
using CubeToolsPublicSite.Models;
using CubeToolsPublicSite.EntityBinding;
using CubeToolsPublicSite.Repository;
using System.IO;
using CubeToolsPublicSite.Helpers;

namespace CubeToolsPublicSite.Controllers
{
	public class DownloadController : BaseController
	{
		private readonly CubeFinanceToolsEntities context;
		readonly IUnitOfWork unitofwork;
		private readonly UploadRepository uploadRepository;
		List<CubeDocument> lstCubeDocument = new List<CubeDocument>();
		public DownloadController()
		{
			context = new CubeFinanceToolsEntities();
			this.unitofwork = context as IUnitOfWork;
			uploadRepository = new UploadRepository(unitofwork);

		}
		public ActionResult Index()
		{
			try
			{
				ViewBag.Type = "Download";
				return View();
			}
			catch (Exception ex)
			{
				LogObject.WriteErrorLogWithMessage("Error in Download controller Index", ex);
				return View();
			}

		}
		public ActionResult WhitePaper()
		{
			try
			{
				lstCubeDocument = new List<CubeDocument>();
				lstCubeDocument = uploadRepository.Get().OrderByDescending(i => i.Id).Where(i => i.IsPublic == true && i.DocumentTypeId == Convert.ToInt32(DocumentTypeEnum.DocumentTypeE.WhitePaper)).ToList();
				if (TempData["FileDownloadException"] != null)
				{
					ModelState.AddModelError("error", TempData["FileDownloadException"].ToString());
					return View(lstCubeDocument);
				}
				else
				{
					return View(lstCubeDocument);
				}
			}
			catch (Exception ex)
			{
				ModelState.AddModelError("error", ex.Message);
				LogObject.WriteErrorLogWithMessage("Error in Download controller WhitePaper", ex);
				return View();
			}

		}
		public ActionResult CaseStudies()
		{
			try
			{
				lstCubeDocument = new List<CubeDocument>();
				lstCubeDocument = uploadRepository.Get().OrderByDescending(i => i.Id).Where(i => i.IsPublic == true && i.DocumentTypeId == Convert.ToInt32(DocumentTypeEnum.DocumentTypeE.CaseStudies)).ToList();
				if (TempData["FileDownloadException"] != null)
				{
					ModelState.AddModelError("error", TempData["FileDownloadException"].ToString());
					return View(lstCubeDocument);
				}
				else
				{
					return View(lstCubeDocument);
				}
			}
			catch (Exception ex)
			{
				ModelState.AddModelError("error", ex.Message);
				LogObject.WriteErrorLogWithMessage("Error in Download controller CaseStudies", ex);
				return View();
			}
		}
		public ActionResult Howto()
		{
			try
			{
				lstCubeDocument = new List<CubeDocument>();
				lstCubeDocument = uploadRepository.Get().OrderByDescending(i => i.Id).Where(i => i.IsPublic == true && i.DocumentTypeId == Convert.ToInt32(DocumentTypeEnum.DocumentTypeE.Howto)).ToList();
				if (TempData["FileDownloadException"] != null)
				{
					ModelState.AddModelError("error", TempData["FileDownloadException"].ToString());
					return View(lstCubeDocument);
				}
				else
				{
					return View(lstCubeDocument);
				}
			}
			catch (Exception ex)
			{
				ModelState.AddModelError("error", ex.Message);
				LogObject.WriteErrorLogWithMessage("Error in Download controller CaseStudies", ex);
				return View();
			}
		}
		public ActionResult Download()
		{
			try
			{
				lstCubeDocument = new List<CubeDocument>();
				lstCubeDocument = uploadRepository.Get().OrderByDescending(i => i.Id).Where(i => i.IsPublic == true && i.DocumentTypeId == Convert.ToInt32(DocumentTypeEnum.DocumentTypeE.Download)).ToList();
				if (TempData["FileDownloadException"] != null)
				{
					ModelState.AddModelError("error", TempData["FileDownloadException"].ToString());
					return View(lstCubeDocument);
				}
				else
				{
					return View(lstCubeDocument);
				}

			}
			catch (Exception ex)
			{
				ModelState.AddModelError("error", ex.Message);
				LogObject.WriteErrorLogWithMessage("Error in Download controller DocumentList", ex);
				return View();
			}
		}
		public ActionResult DownloadFile(int documentId, string actionName)
		{
			try
			{
				
				var documentDetils = uploadRepository.Get().FirstOrDefault(d => d.Id == documentId);
				return File(Path.Combine(System.Web.Configuration.WebConfigurationManager.AppSettings["PhysicalFilePath"].ToString(), Path.GetFileName(documentDetils.FilePath)), Helpers.Mime.GetMimeType(documentDetils.FilePath), Path.GetFileName(documentDetils.FilePath));
			}
			catch (Exception ex)
			{
				LogObject.WriteErrorLogWithMessage("Error in Download controller DocumentList", ex);
				TempData["FileDownloadException"] = ex.Message;
				return RedirectToAction(actionName);
				//return Json(new
				//{
				//	Result = "ERROR",
				//	Message = ex.Message,
				//	Title = "DownloadFile"

				//}, JsonRequestBehavior.AllowGet);
			}
		}


	}
}