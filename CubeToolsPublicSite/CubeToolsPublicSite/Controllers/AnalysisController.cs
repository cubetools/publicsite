﻿using System;
using System.Web.Mvc;
using log4net;
using _4Log.Log;
namespace CubeToolsPublicSite.Controllers
{
	public class AnalysisController : BaseController
	{
		public ActionResult VolatilityAnalysis()
		{
			try
			{
				return View();
			}
			catch (Exception ex)
			{
				ModelState.AddModelError("error", ex.Message);
				LogObject.WriteErrorLogWithMessage("Error in Volatility Analysis", ex);
				return View();
			}

		}
		public ActionResult VaRAnalysis()
		{
			try
			{
				return View();
			}
			catch (Exception ex)
			{
				ModelState.AddModelError("error", ex.Message);
				LogObject.WriteErrorLogWithMessage("Error in  VaR Analysis", ex);
				return View();
			}
			

		}
		public ActionResult SensitivityAnalysis()
		{
			try
			{
				return View();
			}
			catch (Exception ex)
			{
				ModelState.AddModelError("error", ex.Message);
				LogObject.WriteErrorLogWithMessage("Error in  Sensitivity Analysis", ex);
				return View();
			}
			

		}
	}
}