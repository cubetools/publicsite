﻿using _4Log.Log;
using log4net;
using System;
using System.Web.Mvc;

namespace CubeToolsPublicSite.Controllers
{
	public class CustomerTypeController : BaseController
	{
		// GET: CustomerType
		public ActionResult Index(string displayType)
		{
			try
			{
				//ViewBag.DisplayType = displayType;
				return View();
			}
			catch (Exception ex)
			{
				ModelState.AddModelError("error", ex.Message);
				LogObject.WriteErrorLogWithMessage("Error in Customer Type", ex);
			}
			return null;
		}
		public ActionResult AssetManagers()
		{
			try
			{
				return View();
			}
			catch (Exception ex)
			{
				ModelState.AddModelError("error", ex.Message);
				LogObject.WriteErrorLogWithMessage("Error in Asset Managers", ex);
				return View();
			}

		}
		public ActionResult Brokers()
		{
			try
			{
				return View();
			}
			catch (Exception ex)
			{
				ModelState.AddModelError("error", ex.Message);
				LogObject.WriteErrorLogWithMessage("Error in Brokers", ex);
				return View();
			}

		}
		public ActionResult Bankers()
		{
			try
			{
				return View();
			}
			catch (Exception ex)
			{
				ModelState.AddModelError("error", ex.Message);
				LogObject.WriteErrorLogWithMessage("Error in Bankers", ex);
				return View();
			}

		}
		public ActionResult CustodiansAndAdministrators()
		{
			try
			{
				return View();
			}
			catch (Exception ex)
			{
				ModelState.AddModelError("error", ex.Message);
				LogObject.WriteErrorLogWithMessage("Error in Custodians And Administrators", ex);
				return View();
			}

		}
		public ActionResult HedgeFundsAndMultiManagers()
		{
			try
			{
				return View();
			}
			catch (Exception ex)
			{
				ModelState.AddModelError("error", ex.Message);
				LogObject.WriteErrorLogWithMessage("Error in Hedge Funds And Multi Managers", ex);
				return View();
			}

		}
		public ActionResult PensionFunds()
		{
			try
			{
				return View();
			}
			catch (Exception ex)
			{
				ModelState.AddModelError("error", ex.Message);
				LogObject.WriteErrorLogWithMessage("Error in Pension Funds", ex);
				return View();
			}

		}
		public ActionResult PrivateWealth()
		{
			try
			{
				return View();
			}
			catch (Exception ex)
			{
				ModelState.AddModelError("error", ex.Message);
				LogObject.WriteErrorLogWithMessage("Error in Private Wealth", ex);
				return View();
			}

		}
	}
}