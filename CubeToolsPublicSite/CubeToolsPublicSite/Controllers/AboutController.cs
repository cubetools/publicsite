﻿using _4Log.Log;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CubeToolsPublicSite.Controllers
{
	public class AboutController : BaseController
	{
		// GET: About
		public ActionResult Index()
		{
			return View();
		}
		public ActionResult Company()
		{
			try
			{
				return View();
			}

			catch (Exception ex)
			{
				ModelState.AddModelError("error", ex.Message);
				LogObject.WriteErrorLogWithMessage("Error in Company", ex);
				return View();
			}

		}
		public ActionResult OurTeam()
		{
			try
			{
				return View();
			}

			catch (Exception ex)
			{
				ModelState.AddModelError("error", ex.Message);
				LogObject.WriteErrorLogWithMessage("Error in OurTeam", ex);
				return View();
			}

		}
		public ActionResult Expertise()
		{
			try
			{
				return View();
			}

			catch (Exception ex)
			{
				ModelState.AddModelError("error", ex.Message);
				LogObject.WriteErrorLogWithMessage("Error in Expertise", ex);
				return View();
			}

		}

		public ActionResult Security()
		{
			try
			{
				return View();
			}

			catch (Exception ex)
			{
				ModelState.AddModelError("error", ex.Message);
				LogObject.WriteErrorLogWithMessage("Error in Security", ex);
				return View();
			}

		}
		public ActionResult Technology()
		{
			try
			{
				return View();
			}

			catch (Exception ex)
			{
				ModelState.AddModelError("error", ex.Message);
				LogObject.WriteErrorLogWithMessage("Error in Technology", ex);
				return View();
			}

		}
		public ActionResult Portfolio()
		{
			try
			{
				return View();
			}

			catch (Exception ex)
			{
				ModelState.AddModelError("error", ex.Message);
				LogObject.WriteErrorLogWithMessage("Error in Portfolio", ex);
				return View();
			}

		}
		public ActionResult Careers()
		{
			try
			{
				return View();
			}

			catch (Exception ex)
			{
				ModelState.AddModelError("error", ex.Message);
				LogObject.WriteErrorLogWithMessage("Error in Careers", ex);
				return View();
			}

		}

		public ActionResult CustomerSupport()
		{
			try
			{
				return View();
			}

			catch (Exception ex)
			{
				ModelState.AddModelError("error", ex.Message);
				LogObject.WriteErrorLogWithMessage("Error in Careers", ex);
				return View();
			}

		}
		
		public ActionResult Clients()
		{
			try
			{
				return View();
			}
			catch (Exception ex)
			{
				ModelState.AddModelError("error", ex.Message);
				LogObject.WriteErrorLogWithMessage("Error in Clients view", ex);
				return View();
			}
		}
	}
}