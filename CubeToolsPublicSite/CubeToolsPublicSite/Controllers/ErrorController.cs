﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CubeToolsPublicSite.Controllers
{
    public class ErrorController : Controller
    {
        // GET: Error
        [HandleError]
        public ActionResult Index(ExceptionContext exceptionContext)
        {
            return PartialView("Error");
        }
		public ActionResult HttpError404()
		{
			return View();
		}
		public ActionResult HttpError403()
		{
			return View();
		}
		public ActionResult HttpError500()
		{
			return View();
		}
	}
}