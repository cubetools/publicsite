﻿using _4Log.Log;
using System;
using System.Web.Mvc;
using log4net;
using CubeToolsPublicSite.Models;
using System.Collections.Generic;
using System.Linq;
using CubeToolsPublicSite.EntityBinding;
using CubeToolsPublicSite.Repository;
namespace CubeToolsPublicSite.Controllers
{

	public class MarginsController : BaseController
	{
		private CubeFinanceToolsEntities context;
		IUnitOfWork unitofwork;
		private MarketLocationRepository marketLocationRepository;

		public MarginsController()
		{
			context = new CubeFinanceToolsEntities();
			this.unitofwork = context as IUnitOfWork;
			marketLocationRepository = new MarketLocationRepository(unitofwork);

		}

		// GET: Margins
		public ActionResult Index()
		{
			try
			{
				return View();
			}
			catch (Exception ex)
			{
				ModelState.AddModelError("error", ex.Message);
				LogObject.WriteErrorLogWithMessage("Error in margins", ex);
				return View();
			}
		}
		//public ActionResult Market()
		//{
		//	try
		//	{
		//		return View();
		//	}
		//	catch (Exception ex)
		//	{
		//		LogObject.WriteErrorLogWithMessage("Error in markets", ex);
		//	}
		//	return null;
		//}

		//public ActionResult AllMarketLocation()
		//{
		//	try
		//	{

		//		var marketLocationList = marketLocationRepository.Get().Select(m => new { Description = m.ToolTipDescription, City = m.City, GeoLat = m.Latitude, GeoLong = m.Longitude, base64string = Convert.ToBase64String(m.MarkerImage) });
		//		return Json(marketLocationList, JsonRequestBehavior.AllowGet);
		//	}
		//	catch (Exception ex)
		//	{
		//		LogObject.WriteErrorLogWithMessage("Error in AllMarketLocation", ex);
		//		LogObject.WriteErrorLog(ex);
		//		return Json(new
		//		{
		//			Result = "ERROR",
		//			Message = SharedResources.Resources.ErrorMessage,
		//			Title = SharedResources.Resources.ErrorMessageTitle

		//		}, JsonRequestBehavior.AllowGet);
		//	}
		//}
	}
}