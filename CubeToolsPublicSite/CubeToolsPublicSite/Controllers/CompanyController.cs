﻿using System.Web.Mvc;
using log4net;
using System;
using _4Log.Log;

namespace CubeToolsPublicSite.Controllers
{
	public class CompanyController : BaseController
	{
		public ActionResult Index()
		{
			try
			{
				return View();
			}

			catch (Exception ex)
			{
				ModelState.AddModelError("error", ex.Message);
				LogObject.WriteErrorLogWithMessage("Error in Company", ex);
				return View();
			}

		}
	}
}