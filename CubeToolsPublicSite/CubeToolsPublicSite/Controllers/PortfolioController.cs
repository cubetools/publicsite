﻿using _4Log.Log;
using System;
using System.Web.Mvc;
using log4net;
namespace CubeToolsPublicSite.Controllers
{
	public class PortfolioController : BaseController
	{
		// GET: Portfolio
		public ActionResult Index()
		{
			try
			{
				return View();
			}
			catch (Exception ex)
			{
				ModelState.AddModelError("error", ex.Message);
				LogObject.WriteErrorLogWithMessage("Error in  Portfolio", ex);
				return View();
			}

		}
	}
}