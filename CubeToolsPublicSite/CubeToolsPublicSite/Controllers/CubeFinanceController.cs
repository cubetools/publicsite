﻿using _4Log.Log;
using System;
using System.Web.Mvc;
using log4net;
namespace CubeToolsPublicSite.Controllers
{
	public class CubeFinanceController : BaseController
	{
		// GET: CubeFinance
		public ActionResult Index()
		{
			try
			{
				return View();
			}
			catch (Exception ex)
			{
				ModelState.AddModelError("error", ex.Message);
				LogObject.WriteErrorLogWithMessage("Error in  CubeFinance Index", ex);
				return View();
			}
		}
	}
}