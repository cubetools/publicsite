﻿using _4Log.Log;
using CubeToolsPublicSite.EntityBinding;
using CubeToolsPublicSite.Models;
using CubeToolsPublicSite.Repository;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CubeToolsPublicSite.Controllers
{
	public class CubeSoftwareController : BaseController
	{
		private readonly CubeFinanceToolsEntities context;
		readonly IUnitOfWork unitofwork;
		readonly private MarketLocationRepository marketLocationRepository;

		public CubeSoftwareController()
		{
			context = new CubeFinanceToolsEntities();
			this.unitofwork = context as IUnitOfWork;
			marketLocationRepository = new MarketLocationRepository(unitofwork);

		}
		
		// GET: CubeSoftware
		public ActionResult Index()
		{
			try
			{
				ViewBag.Type = "CubeSoftware";
				return View();
			}
			catch (Exception ex)
			{
				ModelState.AddModelError("error", ex.Message);
				LogObject.WriteErrorLogWithMessage("Error in CubeSoftware Index", ex);
				return View();
			}
		}
		public ActionResult Overview()
		{
			try
			{
				return View();
			}
			catch (Exception ex)
			{
				ModelState.AddModelError("error", ex.Message);
				LogObject.WriteErrorLogWithMessage("Error in CubeSoftware Overview", ex);
				return View();
			}
		}
		public ActionResult CubeRiskManagementOverview()
		{
			try
			{
				return View();
			}
			catch (Exception ex)
			{
				ModelState.AddModelError("error", ex.Message);
				LogObject.WriteErrorLogWithMessage("Error in CubeSoftware CubeRiskManagementOverview", ex);
				return View();
			}
		}
		public ActionResult CubeRiskManagementFeatures()
		{
			try
			{
				return View();
			}
			catch (Exception ex)
			{
				ModelState.AddModelError("error", ex.Message);
				LogObject.WriteErrorLogWithMessage("Error in CubeSoftware CubeRiskManagementFeatures", ex);
				return View();
			}
		}

		public ActionResult Coverage()
		{
			try
			{
				return View();
			}
			catch (Exception ex)
			{
				ModelState.AddModelError("error", ex.Message);
				LogObject.WriteErrorLogWithMessage("Error in CubeSoftware Coverage", ex);
				return View();
			}
		}


		public ActionResult PortfolioCubeOverview()
		{
			try
			{
				return View();
			}
			catch (Exception ex)
			{
				ModelState.AddModelError("error", ex.Message);
				LogObject.WriteErrorLogWithMessage("Error in CubeSoftware PortfolioCubeOverview", ex);
				return View();
			}
		}
		public ActionResult PortfolioCubeFeatures()
		{
			try
			{
				return View();
			}
			catch (Exception ex)
			{
				ModelState.AddModelError("error", ex.Message);
				LogObject.WriteErrorLogWithMessage("Error in CubeSoftware PortfolioCubeFeatures", ex);
				return View();
			}
		}
		public ActionResult OptionCubeOverview()
		{
			try
			{
				return View();
			}
			catch (Exception ex)
			{
				ModelState.AddModelError("error", ex.Message);
				LogObject.WriteErrorLogWithMessage("Error in CubeSoftware OptionCubeOverview", ex);
				return View();
			}
		}
		public ActionResult OptionCubeFeatures()
		{
			try
			{
				return View();
			}
			catch (Exception ex)
			{
				ModelState.AddModelError("error", ex.Message);
				LogObject.WriteErrorLogWithMessage("Error in CubeSoftware OptionCubeFeatures", ex);
				return View();
			}
		}
		public ActionResult CubePreTradeOverview()
		{
			try
			{
				return View();
			}
			catch (Exception ex)
			{
				ModelState.AddModelError("error", ex.Message);
				LogObject.WriteErrorLogWithMessage("Error in CubeSoftware CubePreTradeOverview", ex);
				return View();
			}

		}
		public ActionResult CubePreTradeFeatures()
		{
			try
			{
				return View();
			}
			catch (Exception ex)
			{
				ModelState.AddModelError("error", ex.Message);
				LogObject.WriteErrorLogWithMessage("Error in CubeSoftware CubePreTradeFeatures", ex);
				return View();
			}

		}

		public ActionResult AllMarketLocation()
		{
			try
			{
				var marketLocationList = marketLocationRepository.Get().Select(m => new { Description = m.ToolTipDescription, City = m.City, GeoLat = m.Latitude, GeoLong = m.Longitude, base64string = Convert.ToBase64String(m.MarkerImage) });
				return Json(marketLocationList, JsonRequestBehavior.AllowGet);
			}
			catch (Exception ex)
			{
				LogObject.WriteErrorLogWithMessage("Error in AllMarketLocation", ex);
				return Json(new
				{
					Result = "ERROR",
					Message = ex.Message,
					Title = "error"

				}, JsonRequestBehavior.AllowGet);
			}
		}
	}

}