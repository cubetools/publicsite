﻿var LocationData = [];
var LocationLatLon = [];

$(document).ready(function () {
	 var UrlGetMaretLocation = $('input[name="UrlGetMaretLocation"]').val();
	 $.ajax({
	 	 type: "GET",
	 	 url: UrlGetMaretLocation,
	 	 contentType: "application/octet-stream",
	 	 dataType: "json",
	 	 async: false,
	 	 // cache: false,
	 	 success: function (response) {

	 	 	 if (!response || (response.Result && response.Result == "ERROR")) {
	 	 	 	 new Notification(response.Message, response.Title, 'error');
	 	 	 }
	 	 	 else {
	 	 	 	 var t1 = response;
	 	 	 	 if (t1 != "undefined" && t1 != "" && t1 != null) {
	 	 	 	 	 $(t1).each(function (i, v) {
	 	 	 	 	 	 LocationData.push([v.Description, v.City, v.GeoLat, v.GeoLong, v.base64string]);
	 	 	 	 	 	 LocationLatLon.push([v.GeoLat, v.GeoLong]);
	 	 	 	 	 });
	 	 	 	 }
	 	 	 }
	 	 }
	 });
	 Initialize();

});

// Where all the fun happens
function Initialize() {

	 // Google has tweaked their interface somewhat - this tells the api to use that new UI
	 google.maps.visualRefresh = true;
	 var Liverpool = new google.maps.LatLng(8.862772, 46.002205);
	
	 // These are options that set initial zoom level, where the map is centered globally to start, and the type of map to show
	 var mapOptions = {
	 	 zoom: 2,
	 	 center: Liverpool,
	 	 // mapTypeId: google.maps.MapTypeId.G_NORMAL_MAP,
	 	 mapTypeId: google.maps.MapTypeId.ROADMAP,
	 	 scrollwheel: false,
	 	 navigationControl: false,
	 	 mapTypeControl: false,
	 	 scaleControl: false,
	 	 draggable: false,
	 	 streetViewControl: false,
	 	 panControl: false,
	 	 zoomControl: false,
	 	 overviewMapControl: false,
	 	 disableDoubleClickZoom: true,
	 };

	 // This makes the div with id "map_canvas" a google map
	 var map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
	 var currentInfoWindow = null;

	 // Using the JQuery "each" selector to iterate through the JSON list and drop marker pins
	 $.each(LocationData, function (i, item) {

	 	 var marker = new google.maps.Marker({
	 	 	 'position': new google.maps.LatLng(item[2], item[3]),
	 	 	 'map': map,
	 	 	 'title': item[0],
	 	 	 "city": item[1],
	 	 	 icon: {
	 	 	 	 url: "data:image/png;base64," + item[4]
	 	 	 	 //,//size: new google.maps.Size(50, 50),
	 	 	 },
	 	 	 animation: google.maps.Animation.DROP
	 	 });

	 	 // Make the marker-pin blue!
	 	 //  marker.setIcon("data:image/png;base64," + item[4] + "")

	 	 // put in some information about each json object - in this case, the opening hours.
	 	 var infowindow = new google.maps.InfoWindow({
	 	 	 //content: "<div class='infoDiv'><h2 class='divFont'>" + item[0] + "</h2>" + "<div><h4>City: " + item[1] + "</h4></div>" + "<div><h4>Lat: " + item[2] + "</h4></div>" + "<div><h4>Lon: " + item[3] + "</h4></div></div>"
	 	 	 content: "<div class='infoDiv'><h2 class='divFont'>" + item[0] + "</h2>" + "<div><h4>City: " + item[1] + "</h4></div></div>"
	 	 });

	 	 // finally hook up an "OnClick" listener to the map so it pops up out info-window when the marker-pin is clicked!
	 	 google.maps.event.addListener(marker, 'click', function () {
	 	 	 infowindow.open(map, marker);
	 	 });


	 	 google.maps.event.addListener(marker, 'click', function () {
	 	 	 if (currentInfoWindow != null) {
	 	 	 	 currentInfoWindow.close();
	 	 	 }
	 	 	 infowindow.open(map, marker);
	 	 	 currentInfoWindow = infowindow;
	 	 });
	 })
}


