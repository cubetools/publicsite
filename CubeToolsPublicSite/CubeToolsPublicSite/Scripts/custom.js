﻿function alertdialog(outputMsg, titleMsg, onCloseCallback) {
    if (!titleMsg)
        titleMsg = 'Message';

    if (!outputMsg)
        outputMsg = 'No Message to Display.';

    $("<div id='alertdialog'></div>").html(outputMsg).dialog({
        title: titleMsg,
        resizable: true,
        modal: true,
        buttons: {
            "OK": function () {
                $(this).dialog("destroy");
            }
        },
        close: onCloseCallback
    });
}
function confirmdialog(outputMsg, titleMsg, context, onOkCallback) {
    var defer = $.Deferred();
    if (!titleMsg)
        titleMsg = 'Confirm';

    if (!outputMsg)
        outputMsg = 'No Message to Display.';

    $("<div id='confirmdialog'></div>").html(outputMsg).dialog({
        title: titleMsg,
        resizable: true,
        modal: true,
        buttons: [{
            text: "Cancel",
            click: function () {
                context.confirm = false;
                defer.resolve(context);
                $(this).dialog("destroy");
            }
        }, {
            text: "OK",
            click: function () {
                context.confirm = true;
                defer.resolve(context);
                $(this).dialog("destroy");
                onOkCallback;
            }
        }]
    });
    return defer.promise();
}
