﻿using CubeToolsPublicSite.EntityBinding;
using CubeToolsPublicSite.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CubeToolsPublicSite.Repository
{
    public class MarketLocationRepository : GenericRepository<MarketLocation>
    {
        CubeFinanceToolsEntities _context;

        public MarketLocationRepository(IUnitOfWork unitofwork)
            : base(unitofwork as CubeFinanceToolsEntities)
        {
            if (unitofwork == null)
            {
                throw new ArgumentException("unitofwork");
                _context = unitofwork as CubeFinanceToolsEntities;
            }
        }
    }
}