﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(CubeToolsPublicSite.Startup))]
[assembly: log4net.Config.XmlConfigurator(ConfigFile = "log4net.config", Watch = true)]
namespace CubeToolsPublicSite
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
