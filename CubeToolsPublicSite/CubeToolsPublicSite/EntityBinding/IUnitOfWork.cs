﻿using System;
namespace CubeToolsPublicSite.EntityBinding
{
    public interface IUnitOfWork : IDisposable
    {
        void Save();
    }
}