﻿using CubeToolsPublicSite.Models;
using System;
namespace CubeToolsPublicSite.EntityBinding
{
    public class UnitOfWork : IUnitOfWork
    {
        readonly CubeFinanceToolsEntities _context = new CubeFinanceToolsEntities();
        public void Save()
        {
            _context.SaveChanges();
        }

        private bool _disposed;

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            _disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}