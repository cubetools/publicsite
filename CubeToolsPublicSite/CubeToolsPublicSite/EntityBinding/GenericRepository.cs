﻿using CubeToolsPublicSite.Models;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace CubeToolsPublicSite.EntityBinding
{
    public class GenericRepository<TEntity> where TEntity : class
    {
        internal CubeFinanceToolsEntities context;
        internal DbSet<TEntity> dbset;
        public GenericRepository(CubeFinanceToolsEntities context)
        {
            this.context = context;
            this.dbset = context.Set<TEntity>();
        }
        public virtual IEnumerable<TEntity> Get()
        {
            IQueryable<TEntity> query = dbset;
            return query.ToList();
        }
        public virtual TEntity GetByID(object id)
        {
            return dbset.Find(id);
        }
        public virtual void Insert(TEntity entity)
        {
            dbset.Add(entity);
        }
        public virtual void Delete(TEntity entitytodelete)
        {
            dbset.Attach(entitytodelete);
            dbset.Remove(entitytodelete);
        }
        public virtual void DeleteAll(TEntity entitytodelete)
        {
            dbset.Attach(entitytodelete);
            dbset.Remove(entitytodelete);
        }
        public virtual void Update(TEntity entitytoupdate)
        {
            dbset.Attach(entitytoupdate);
            context.Entry(entitytoupdate).State = EntityState.Modified;
        }
        public virtual void Detach(TEntity OldentityToDetach)
        {
            context.Entry<TEntity>(OldentityToDetach).State = System.Data.Entity.EntityState.Detached;
        }
        public virtual IEnumerable<TEntity> GetWithRawSql(string query, params object[] parameters)
        {
            return dbset.SqlQuery(query, parameters).ToList();
        }
    }
}