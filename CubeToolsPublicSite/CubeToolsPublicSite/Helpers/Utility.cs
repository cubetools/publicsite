﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Web.Mvc;
namespace CubeToolsPublicSite.Helpers
{
    public class Utility
    {
        public static string GetHtmlTitle(string Html)
        {
            string Title = string.Empty;
            int Start = 0;
            int Length = 0;
            Start = Html.IndexOf("<title>", 0, StringComparison.OrdinalIgnoreCase);
            if (Start == -1) return null;
            Start += 7;
            Length = Html.IndexOf("</title>", 0, StringComparison.OrdinalIgnoreCase) - Start;
            if (Length <= 0) return null;
            Title = Html.Substring(Start, Length);
            Title = System.Text.RegularExpressions.Regex.Replace(Title, "\\s+", " ");
            return Title = Title.Trim();
        }
        public static string GetHtml(ControllerContext controllercontext, string viewName, object model)
        {

            controllercontext.Controller.ViewData.Model = model;

            var result = ViewEngines.Engines.FindView(controllercontext, viewName, null);

            StringWriter output;
            controllercontext.Controller.ViewData["Model"] = model;
            using (output = new StringWriter())
            {
                var viewContext = new ViewContext(controllercontext, result.View, controllercontext.Controller.ViewData, controllercontext.Controller.TempData, output);
                result.View.Render(viewContext, output);
                result.ViewEngine.ReleaseView(controllercontext, result.View);
            }

            return output.ToString();
        }


    }
}