﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CubeToolsPublicSite.Helpers
{
	public static class DocumentTypeEnum
	{
	  public enum DocumentTypeE
		{
			WhitePaper = 1,
			CaseStudies = 2,
			Howto = 3,
			Other = 4,
			Download = 5
		};
	}
}