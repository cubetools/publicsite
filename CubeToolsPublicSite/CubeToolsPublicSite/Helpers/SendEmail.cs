﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Mail;
namespace CubeToolsPublicSite.Helpers
{
    public static class SendEmail
    {
        public static string SendEmailNew(List<string> EmailTo, List<string> NameTo, List<string> EmailCCTo, List<string> NameCCTo, List<string> EmailBccTo, List<string> NameBccTo, string NameReplyTo, string EmailReplyTo, string Subject, string Body, List<string> Attachments)
        {
            string mailStatus = string.Empty;
            string From = System.Configuration.ConfigurationManager.AppSettings["From"].ToString();
            string UserName = System.Configuration.ConfigurationManager.AppSettings["UserName"].ToString();
            string Password = System.Configuration.ConfigurationManager.AppSettings["Password"].ToString();
            string Port = System.Configuration.ConfigurationManager.AppSettings["Port"].ToString();
            string Host = System.Configuration.ConfigurationManager.AppSettings["Host"].ToString();
            MailMessage Mail = null;
            try
            {
                if (System.Web.HttpContext.Current != null)
                {
                    string BaseUrl = AbsolutizeUrl("~/");
                    Body = Body.Replace("\"~/", "\"" + BaseUrl);
                }
                Mail = new MailMessage();
                Mail.From = new MailAddress(From);
                // To
                for (int i = 0; i < EmailTo.Count; i++)
                {
                    if (!string.IsNullOrEmpty(EmailTo[i]))
                    {
                        Mail.To.Add(new MailAddress(EmailTo[i]));
                    }
                }
                //CC
                if (EmailCCTo != null && EmailCCTo.Count > 0)
                {
                    for (int i = 0; i < EmailCCTo.Count; i++)
                    {
                        if (!string.IsNullOrEmpty(EmailCCTo[i]))
                        {
                            Mail.CC.Add(new MailAddress(EmailCCTo[i]));
                        }
                    }
                }
                //Bcc
                if (EmailBccTo != null && EmailBccTo.Count > 0)
                {

                    for (int i = 0; i < EmailBccTo.Count; i++)
                    {
                        if (!string.IsNullOrEmpty(EmailBccTo[i]))
                        {
                            Mail.Bcc.Add(new MailAddress(EmailBccTo[i]));
                        }
                    }
                }
                // Reply To
                if (NameReplyTo != null && EmailReplyTo != null)
                {
                    Mail.ReplyTo = new MailAddress(EmailReplyTo, NameReplyTo);
                }
                //Attachments
                if (Attachments != null && Attachments.Count > 0)
                {
                    foreach (string Attachment in Attachments)
                    {
                        string Path = Attachment;
                        if (Path.IndexOf("~") > -1)
                        {
                            Path = System.Web.HttpContext.Current.Server.MapPath(Path);
                        }
                        Mail.Attachments.Add(new System.Net.Mail.Attachment(Path));

                    }
                }
                Mail.IsBodyHtml = true;
                Mail.Body = Body;
                Mail.Subject = Subject;
                SmtpClient smtpClient = new SmtpClient();
                smtpClient.Host = Host;
                smtpClient.EnableSsl = true;
                NetworkCredential netCredential = new NetworkCredential();
                netCredential.UserName = UserName;
                netCredential.Password = Password;
                smtpClient.UseDefaultCredentials = true;
                smtpClient.Credentials = netCredential;
                smtpClient.Port = Convert.ToInt32(Port);
                smtpClient.Send(Mail);
                mailStatus = "true";
                return mailStatus;
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error sending email", ex);
                return "error";
            }
            finally
            {
                if ((Mail != null))
                    Mail.Dispose();
                Mail = null;
            }
        }
        public static string AbsolutizeUrl(string strUrl)
        {
            if (strUrl.StartsWith("~"))
            {
                strUrl = RootUrl() + strUrl.Substring(1).Replace("//", "/");
            }
            return strUrl;
        }
        private static string RootUrl()
        {
            string strProtocol = null;
            string strPort = null;
            string strServerName = null;
            string strApplicationPath = null;
            if ((System.Web.HttpContext.Current != null))
            {
                // Get the protocol
                strProtocol = System.Web.HttpContext.Current.Request.ServerVariables["SERVER_PORT_SECURE"];
                strProtocol = strProtocol == null | strProtocol == "0" ? "http://" : "https://";
                // Get the port
                strPort = System.Web.HttpContext.Current.Request.ServerVariables["SERVER_PORT"];
                strPort = strPort == null | strPort == "80" | strPort == "443" ? "" : ":" + strPort;
                strServerName = System.Web.HttpContext.Current.Request.ServerVariables["SERVER_NAME"];
                strApplicationPath = System.Web.HttpContext.Current.Request.ApplicationPath;
            }
            if
                (strApplicationPath.EndsWith("/"))
                strApplicationPath = strApplicationPath.Substring(0, strApplicationPath.Length - 1);
            return strProtocol + strServerName + strPort + strApplicationPath;
        }
    }
}