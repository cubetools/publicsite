﻿using _4Log.Log;
using CubeToolsPublicSite.Controllers;
using CubeToolsPublicSite.Helpers;
using log4net;
using log4net.Config;
using System;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
namespace CubeToolsPublicSite
{
    public class MvcApplication : HttpApplication
    {
        public string rootPath = string.Empty;
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }

        protected void Session_Start()
        {
            rootPath = HttpContext.Current.Request.PhysicalApplicationPath;
            XmlConfigurator.Configure(new FileInfo(Path.Combine(rootPath, ConfigurationManager.AppSettings["log4netConfigFile"])));
        }

        public void Application_BeginRequest(object sender, EventArgs e)
        {
            string cultureName = null;
            HttpCookie cultureCookie = Request.Cookies["_culture"];
            if (cultureCookie != null)
            {
                cultureName = cultureCookie.Value;
            }
            else
            {
                cultureName = Request.UserLanguages != null && Request.UserLanguages.Length > 0 ? Request.UserLanguages[0] : null; // obtain it from HTTP header AcceptLanguages
                cultureName = CultureHelper.GetImplementedCulture(cultureName);
                //Set cookie
                cultureCookie = new HttpCookie("_culture");
                cultureCookie.Value = cultureName;
                cultureCookie.Expires = DateTime.Now.AddYears(1);
                Response.Cookies.Add(cultureCookie);
            }

            Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(cultureName);
            Thread.CurrentThread.CurrentUICulture = Thread.CurrentThread.CurrentCulture;
        }

		protected void Application_Error()
		{

			if (Context.IsCustomErrorEnabled)
			{
				var exception = Server.GetLastError();
				var httpException = exception as HttpException;
				ShowCustomErrorPage(httpException);
			}


		}
		private void ShowCustomErrorPage(Exception exception)
		{
			var httpException = exception as HttpException ?? new HttpException(500, "Internal Server Error", exception);

			Response.Clear();
			var routeData = new RouteData();
			routeData.Values.Add("controller", "Error");
			routeData.Values.Add("fromAppErrorEvent", true);

			switch (httpException.GetHttpCode())
			{
				case 403:
					routeData.Values.Add("action", "HttpError403");
					LogObject.WriteErrorLogWithMessage("403 error", exception);
					break;

				case 404:
					routeData.Values.Add("action", "HttpError404");
					LogObject.WriteErrorLogWithMessage("404 error", exception);
					break;

				case 500:
					routeData.Values.Add("action", "HttpError500");
					LogObject.WriteErrorLogWithMessage("500 error", exception);
					break;

				default:
					routeData.Values.Add("action", "GeneralError");
					routeData.Values.Add("httpStatusCode", httpException.GetHttpCode());
					break;
			}

			Server.ClearError();

			IController controller = new ErrorController();
			controller.Execute(new RequestContext(new HttpContextWrapper(Context), routeData));
		}
	}
}
