﻿using CubeToolsPublicSite.EntityBinding;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CubeToolsPublicSite.Models
{
    public partial class CubeFinanceToolsEntities : IUnitOfWork
    {
        public void Save()
        {
            SaveChanges();
        }
    }
}