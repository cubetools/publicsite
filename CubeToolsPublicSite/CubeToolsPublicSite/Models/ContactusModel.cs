﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace CubeToolsPublicSite.Models
{
    public class ContactusModel
    {

        [Display(Name = "Name", ResourceType = typeof(SharedResources.Resources))]
        [Required(ErrorMessageResourceType = typeof(SharedResources.Resources), ErrorMessageResourceName = "ValidateName")]
        public string Name { get; set; }

        [Display(Name = "Surname", ResourceType = typeof(SharedResources.Resources))]
        [Required(ErrorMessageResourceType = typeof(SharedResources.Resources), ErrorMessageResourceName = "ValidateSurname")]
        public string Surname { get; set; }

        [Display(Name = "CompanyName", ResourceType = typeof(SharedResources.Resources))]
        [Required(ErrorMessageResourceType = typeof(SharedResources.Resources), ErrorMessageResourceName = "ValidateCompany")]
        public string CompanyName { get; set; }

        [Display(Name = "IssuerType", ResourceType = typeof(SharedResources.Resources))]
        [Required(ErrorMessageResourceType = typeof(SharedResources.Resources), ErrorMessageResourceName = "ValidateIssuerType")]
        public string IssuerType { get; set; }

        [Display(Name = "IssuerTypeList", ResourceType = typeof(SharedResources.Resources))]
        [Required(ErrorMessageResourceType = typeof(SharedResources.Resources), ErrorMessageResourceName = "ValidateIssuerType")]
        public List<SelectListItem> IssuerTypeList { get; set; }

        [Display(Name = "CompanyTypeList", ResourceType = typeof(SharedResources.Resources))]
        [Required(ErrorMessageResourceType = typeof(SharedResources.Resources), ErrorMessageResourceName = "ValidateCompanyType")]
        public List<SelectListItem> CompanyTypeList { get; set; }


        [DataType(DataType.EmailAddress)]
        [Display(Name = "Email", ResourceType = typeof(SharedResources.Resources))]
        [Required(ErrorMessageResourceType = typeof(SharedResources.Resources), ErrorMessageResourceName = "ValidateEmail")]
        public string Email { get; set; }

        [Display(Name = "CompanyType", ResourceType = typeof(SharedResources.Resources))]
        [Required(ErrorMessageResourceType = typeof(SharedResources.Resources), ErrorMessageResourceName = "ValidateCompanyType")]
        public string CompanyType { get; set; }


       
        [StringLength(16, MinimumLength = 16)]

        [Display(Name = "Mobile", ResourceType = typeof(SharedResources.Resources))]
        // [Required(ErrorMessageResourceType = typeof(SharedResources.Resources), ErrorMessageResourceName = "ValidatePhoneNumber")]
        //[RegularExpression(@"^(\d{13})$", ErrorMessageResourceType = typeof(SharedResources.Resources), ErrorMessageResourceName = "ValidatePhoneNumber")]
        public string Mobile { get; set; }


        [Display(Name = "Message", ResourceType = typeof(SharedResources.Resources))]
        [Required(ErrorMessageResourceType = typeof(SharedResources.Resources), ErrorMessageResourceName = "ValidateMessage")]
        public string Message { get; set; }

        [Display(Name = "SelectMessage", ResourceType = typeof(SharedResources.Resources))]
        public string SelectMessage { get; set; }

        public ContactusModel()
        {
            IssuerTypeList = new List<SelectListItem>();
            CompanyTypeList = new List<SelectListItem>();
        }
    }
}