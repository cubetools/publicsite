﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CubeToolsPublicSite.Models
{
    public class Menus
    {
        [Display(Name = "Home", ResourceType = typeof(SharedResources.Resources))]
        public string Home { get; set; }

        [Display(Name = "Company", ResourceType = typeof(SharedResources.Resources))]
        public string Company { get; set; }

        [Display(Name = "CubeFinanceTools", ResourceType = typeof(SharedResources.Resources))]
        public string CubeFinanceTools { get; set; }

        [Display(Name = "MarginsCalculations", ResourceType = typeof(SharedResources.Resources))]
        public string MarginsCalculations { get; set; }

        [Display(Name = "Portfolio", ResourceType = typeof(SharedResources.Resources))]
        public string Portfolio { get; set; }

        [Display(Name = "Security", ResourceType = typeof(SharedResources.Resources))]
        public string Security { get; set; }

        [Display(Name = "SensitivityAnalysis", ResourceType = typeof(SharedResources.Resources))]
        public string SensitivityAnalysis { get; set; }

        [Display(Name = "VaRAnalysis", ResourceType = typeof(SharedResources.Resources))]
        public string VaRAnalysis { get; set; }

        [Display(Name = "VolatilityAnalysis", ResourceType = typeof(SharedResources.Resources))]
        public string VolatilityAnalysis { get; set; }

        [Display(Name = "Contact", ResourceType = typeof(SharedResources.Resources))]
        public string Contact { get; set; }

        [Display(Name = "SearchString", ResourceType = typeof(SharedResources.Resources))]
        public string SearchString { get; set; }

        [Display(Name = "AboutCubeFinance", ResourceType = typeof(SharedResources.Resources))]
        public string AboutCubeFinance { get; set; }
        [Display(Name = "CubeSoftware", ResourceType = typeof(SharedResources.Resources))]
        public string CubeSoftware { get; set; }
        [Display(Name = "CubeServices", ResourceType = typeof(SharedResources.Resources))]
        public string CubeServices { get; set; }
        [Display(Name = "Consulting", ResourceType = typeof(SharedResources.Resources))]
        public string Consulting { get; set; }
        [Display(Name = "Download", ResourceType = typeof(SharedResources.Resources))]
        public string Download { get; set; }
        [Display(Name = "LoginCubeServices", ResourceType = typeof(SharedResources.Resources))]
        public string LoginCubeServices { get; set; }

    }
}