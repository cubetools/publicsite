﻿function Notification(message, title, type) {

	 toastr.options = {
	 	 "closeButton": true,
	 	 "debug": false,
	 	 //"positionClass": "toast-bottom-full-width",
	 	 "onclick": null,
	 	 "showDuration": "300",
	 	 "hideDuration": "0",
	 	 "timeOut": "0",
	 	 "extendedTimeOut": "0",
	 	 "showEasing": "swing",
	 	 "hideEasing": "linear",
	 	 "showMethod": "fadeIn",
	 	 "hideMethod": "fadeOut"
	 }
	 if (type == 'error') {
	 	 toastr.error(message, title)
	 }
	 else if (type == 'success') {
	 	 toastr.success(message, title)
	 }

}

function TogglePreloader(btnInactive) {

	 if (!$(btnInactive).attr('disabled')) {
	 	 $(btnInactive).attr('disabled', true);
	 }
	 else {
	 	 $(btnInactive).attr('disabled', false);

	 }
}