﻿namespace _4Log.Log
{
    internal class LogCreator : LogCreatorBase
    {
        #region Overrides of LogCreatorBase

        public override LoggerBase CreateLogger()
        {
            return new Logger();
        }

        #endregion
    }
}
