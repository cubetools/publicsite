﻿namespace _4Log.Log
{
    public abstract class LogCreatorBase
    {
        protected LogCreatorBase() { }

        public abstract LoggerBase CreateLogger();
    }
}
