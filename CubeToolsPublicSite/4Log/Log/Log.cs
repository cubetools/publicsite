﻿
namespace _4Log.Log
{
    public class LogObject
    {
        private LogObject() { }


        public static void WriteFatalLog(System.Exception ex)
        {
            LogCreator creator = new LogCreator();
            LoggerBase loggerBase = creator.CreateLogger();
            loggerBase.WriteFatalLog(ex);
        }

        public static void WriteErrorLog(System.Exception ex)
        {
            LogCreator creator = new LogCreator();
            LoggerBase loggerBase = creator.CreateLogger();
            loggerBase.WriteErrorLog(ex);
        }
        public static void WriteErrorLogWithMessage(string message, System.Exception ex)
        {
            LogCreator creator = new LogCreator();
            LoggerBase loggerBase = creator.CreateLogger();
            loggerBase.WriteErrorLogWithMessage(message, ex);
        }

        public static void WriteWarningLog(string message)
        {
            LogCreator creator = new LogCreator();
            LoggerBase loggerBase = creator.CreateLogger();
            loggerBase.WriteWarningLog(message);
        }

        public static void WriteInfoLog(string message)
        {
            if (message.Length == 0)
            {
                return;
            }

            LogCreator creator = new LogCreator();
            LoggerBase loggerBase = creator.CreateLogger();
            loggerBase.WriteInfoLog(message);
        }

        public static void WriteDebugLog(string message)
        {
            LogCreator creator = new LogCreator();
            LoggerBase loggerBase = creator.CreateLogger();
            loggerBase.WriteDebugLog(message);
        }

        // [Step #0.1]
        private static long processId = 0;
        public static long GetProcessId()
        {
            processId++;
            return processId;
        }

        private static string GetMessageWithProcesseId(string message, long id)
        {
            if (id > 0)
            {
                message = string.Format(":Process Id {0}:", id.ToString()) + message;
            }
            return message;
        }

        public static void WriteInfoLog(string message, long id)
        {
            WriteInfoLog(GetMessageWithProcesseId(message, id));
        }

        public static void WriteWarningLog(string message, long id)
        {
            WriteWarningLog(GetMessageWithProcesseId(message, id));
        }

        // [Step #3.0]
        public static void WriteWarningAndInfoLog(string warningMessage, string infoMessage, long id)
        {
            WriteWarningLog(warningMessage, id);
            WriteInfoLog(infoMessage, id);
        }

        public static void WriteWarningAndInfoLog(string warningMessage, string infoMessage)
        {
            WriteWarningAndInfoLog(warningMessage, infoMessage, 0);
        }


        // [Step #2.1]
        public static void WriteEntering(string className, string methodName)
        {
            var message = string.Format("Entering to {0} - Method name: {1}", className, methodName);
            WriteDebugLog(message);
        }

        public static void WriteExiting(string className, string methodName)
        {
            var message = string.Format("Exiting {0} - Method name: {1}", className, methodName);
            WriteDebugLog(message);
        }

        public static void WriteStartWorkflow(string workflowName)
        {
            var message = string.Format("Start invoking workflow: {0}", workflowName);
            WriteDebugLog(message);
        }
        public static void WriteEndWorkflow(string workflowName)
        {
            var message = string.Format("End invoking workflow: {0}", workflowName);
            WriteDebugLog(message);
        }


    }
}
