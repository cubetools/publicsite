﻿using log4net;
namespace _4Log.Log
{
    public sealed class Logger : LoggerBase
    {
        private static readonly ILog logTrace = LogManager.GetLogger("RollingLogFileAppenderForTrace");
        private static readonly ILog logInfo = LogManager.GetLogger("RollingLogFileAppenderForInfo");
        private static readonly ILog logException = LogManager.GetLogger("RollingLogFileAppenderForException");

        public override void WriteFatalLog(System.Exception ex)
        {
            logException.Fatal(ex);
        }

        public override void WriteErrorLog(System.Exception ex)
        {
            logException.Error(ex);
        }

        public override void WriteErrorLogWithMessage(string message, System.Exception ex)
        {
            logException.Error(message, ex);
        }

        public override void WriteWarningLog(string message)
        {
            logInfo.Warn(message);
        }

        public override void WriteInfoLog(string message)
        {
            // [Step #2.0]
            logInfo.Info(message);
        }

        public override void WriteDebugLog(string message)
        {
            logTrace.Debug(message);
        }

    }
}
