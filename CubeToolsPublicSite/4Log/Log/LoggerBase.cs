﻿namespace _4Log.Log
{
    public abstract class LoggerBase
    {
        public abstract void WriteFatalLog(System.Exception ex);
        public abstract void WriteErrorLog(System.Exception ex);
        public abstract void WriteErrorLogWithMessage(string message, System.Exception ex);
        public abstract void WriteWarningLog(string message);
        public abstract void WriteInfoLog(string message);
        public abstract void WriteDebugLog(string message);
    }
}
